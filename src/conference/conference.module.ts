import { Module } from '@nestjs/common';

import { PrismaModule } from 'src/prisma/prisma.module';

import { ConferenceController } from './conference.controller';
import { ConferenceService } from './conference.service';
import { ConferenceGateway } from './conference.gateway';

@Module({
	imports: [PrismaModule],
	controllers: [ConferenceController],
	providers: [ConferenceService, ConferenceGateway]
})
export class ConferenceModule {}
