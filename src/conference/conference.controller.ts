import { Controller, Get, Param, Post, Res, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';

import { IdentifyAccount, JWTAuthGuard } from 'src/shared';
import { PayloadModel } from 'src/user/models';

import { ConferenceService } from './conference.service';

@ApiTags('conference')
@UseGuards(JWTAuthGuard)
@Controller('conference')
export class ConferenceController {
	constructor(private readonly service: ConferenceService) {}

	@Post()
	createConference(@IdentifyAccount() user: PayloadModel, @Res() res: Response) {
		return this.service.createConference(user, res);
	}

	@Get(':id')
	joinConference(@Param('id') id: string, @IdentifyAccount() user: PayloadModel, @Res() res: Response) {
		return this.service.joinConference(id, user, res);
	}

	@Get()
	getConferences(){
		return this.service.getConferences();
	}
}
