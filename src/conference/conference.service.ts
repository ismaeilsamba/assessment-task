import { Injectable, NotFoundException } from '@nestjs/common';
import { Response } from 'express';

import { PrismaService } from 'src/prisma/prisma.service';
import { PayloadModel } from 'src/user/models';

@Injectable()
export class ConferenceService {
	constructor(private repository: PrismaService) {}

	async createConference(user: PayloadModel, res: Response) {
		let conference = await this.repository.conference.create({
			data: {
				url: 'http://localhost:4000',
				creatorUser: {
					connect: {
						id: user.id
					}
				}
			}
		});

		conference = await this.repository.conference.update({
			where: {
				id: conference.id
			},
			data: {
				url: `${conference.url}/conference/${conference.id}`
			}
		});

		return res.send(conference);
		return res.redirect(`/conference/${conference.id}`);
	}

	async joinConference(id: string, userPayload: PayloadModel, res: Response) {
		const conference = await this.repository.conference.findUnique({
			where: {
				id: id
			}
		});
		if (!conference) {
			throw new NotFoundException('conference not found');
		}

		const user = await this.repository.user.findUnique({
			where: {
				id: userPayload.id
			}
		});
		if (!user) {
			throw new NotFoundException('user not found');
		}

		await this.repository.userConference.create({
			data: {
				conference: {
					connect: {
						id: id
					}
				},
				user: {
					connect: {
						id: user.id
					}
				}
			}
		});

		res.render('conference', {
			conferenceId: id
		});
	}

	async getConferences() {
		return this.repository.conference.findMany({
			where: {
				finishedAt: null
			}
		});
	}
}
