export class ConferencePayloadModel {
	userId: number;
	conferenceId: string;
}
