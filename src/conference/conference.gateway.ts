import {
	WebSocketServer,
	WebSocketGateway,
	SubscribeMessage,
	OnGatewayDisconnect,
	OnGatewayConnection
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { Server, Socket } from 'socket.io';

import { PrismaService } from 'src/prisma/prisma.service';

import { ConferencePayloadModel } from './models';

@WebSocketGateway()
export class ConferenceGateway implements OnGatewayConnection, OnGatewayDisconnect {
	constructor(private repository: PrismaService) {}

	@WebSocketServer() server: Server;
	logger = new Logger('ConferenceGateway');

	handleConnection(client: Socket) {
		this.logger.log(`${client.id} has connected successfully.`);
	}

	handleDisconnect(client: Socket) {
		this.logger.log(`${client.id} has disconnected.`);
	}

	@SubscribeMessage('join-conference')
	joinConference(client: Socket, payload: ConferencePayloadModel) {
		client.join(payload.conferenceId);
		client.to(payload.conferenceId).emit('user-connected', {
			userId: payload.userId
		});

		client.on('disconnect', async () => {
			const conference = await this.repository.conference.findUnique({
				where: {
					id: payload.conferenceId
				},
				include: {
					creatorUser: true
				}
			});

			if (conference.creatorUser.id === payload.userId) {
				await this.repository.conference.update({
					where: {
						id: payload.conferenceId
					},
					data: {
						finishedAt: new Date()
					}
				});

				client.to(payload.conferenceId).emit('conference-finished');
			} else {
				client.to(payload.conferenceId).emit('user-disconnected', {
					userId: payload.userId
				});
			}
		});

		this.logger.log(`${payload.userId} has connected to conference ${payload.conferenceId} successfully.`);
	}
}
