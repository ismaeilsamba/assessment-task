import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';

import { ConferenceModule } from 'src/conference/conference.module';
import { PrismaModule } from 'src/prisma/prisma.module';
import { UserModule } from 'src/user/user.module';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
	imports: [
		ConfigModule.forRoot({
			envFilePath: '../../.env',
			isGlobal: true
		}),
		ConferenceModule,
		PrismaModule,
		UserModule
	],
	controllers: [AppController],
	providers: [AppService]
})
export class AppModule {}
