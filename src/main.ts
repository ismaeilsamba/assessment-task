import { NestExpressApplication } from '@nestjs/platform-express';
import { json, urlencoded, static as static_ } from 'express';
import { Logger, ValidationPipe } from '@nestjs/common';
import * as cookieParser from 'cookie-parser';
import { NestFactory } from '@nestjs/core';

import { swaggerConfig } from 'src/shared';

import { AppModule } from './app.module';
import { join } from 'path';

async function bootstrap() {
	const app = await NestFactory.create<NestExpressApplication>(AppModule);

	swaggerConfig(app);

	app.use(
		json({
			limit: '50mb'
		})
	);

	app.use(
		urlencoded({
			limit: '50mb',
			extended: true
		})
	);

	app.useGlobalPipes(
		new ValidationPipe({
			whitelist: true
		})
	);

	app.enableCors({
		allowedHeaders: '*'
	});

	app.use(cookieParser());
	app.set('view engine', 'ejs');
	app.use(static_(join(__dirname, '..', 'public')));

	const port = process.env.PORT || 4000;
	await app.listen(port);

	Logger.log(`🚀 Application is running on: http://localhost:${port}`);
}

bootstrap();
