import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { INestApplication } from '@nestjs/common';

export const swaggerConfig = (app: INestApplication) => {
	SwaggerModule.setup(
		'api',
		app,
		SwaggerModule.createDocument(
			app,
			new DocumentBuilder()
				.setTitle('API Documentation')
				.setDescription('The API docs for misraj-assessment-task project')
				.setVersion('1.0')
				.build(),
			{
				operationIdFactory: (controllerKey: string, methodKey: string) => methodKey
			}
		),
		{
			swaggerOptions: {
				apisSorter: 'alpha',
				operationsSorter: 'alpha',
				tagsSorter: 'alpha'
			}
		}
	);
};
