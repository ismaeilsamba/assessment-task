import * as bcrypt from 'bcrypt';

export const validatePassword = async (input_password: string, true_password: string) => {
	return await bcrypt.compare(input_password, true_password);
};
