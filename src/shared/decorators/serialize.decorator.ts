import { ClassConstructor } from 'class-transformer';
import { UseInterceptors } from '@nestjs/common';

import { SerializeInterceptor } from '../interceptors';

export function Serialize<T>(dto: ClassConstructor<T>) {
	return UseInterceptors(new SerializeInterceptor(dto));
}
