import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Request } from 'express';

import { PayloadModel } from '../models';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
	constructor() {
		super({
			jwtFromRequest: ExtractJwt.fromExtractors([JwtStrategy.extractJWT, ExtractJwt.fromAuthHeaderAsBearerToken()]),
			secretOrKey: process.env.JWT_SECRET,
			ignoreExpiration: false
		});
	}

	private static extractJWT(req: Request): string | null {
		if (req.cookies && 'accessToken' in req.cookies && req.cookies.accessToken.length > 0) {
			return req.cookies.accessToken;
		}
		return null;
	}

	async validate(payload: PayloadModel) {
		return payload;
	}
}
