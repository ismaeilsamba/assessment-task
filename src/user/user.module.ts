import { ConfigModule, ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';

import { PrismaModule } from 'src/prisma/prisma.module';

import { JwtStrategy, LocalStrategy } from './strategies';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
	imports: [
		PrismaModule,
		PassportModule.register({
			session: false
		}),
		JwtModule.registerAsync({
			imports: [ConfigModule],
			inject: [ConfigService],
			useFactory: (config: ConfigService) => ({
				secret: config.get<string>('JWT_SECRET'),
				signOptions: {
					expiresIn: config.get<string>('EXPIRES_IN')
				}
			})
		})
	],
	controllers: [UserController],
	providers: [UserService, JwtStrategy, LocalStrategy]
})
export class UserModule {}
