import { IsEmail, IsString, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
	@IsEmail()
	@ApiProperty({
		required: true
	})
	email: string;

	@IsString()
	@Length(8, 32)
	@ApiProperty({
		required: true
	})
	password: string;
}
