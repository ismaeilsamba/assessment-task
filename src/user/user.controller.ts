import { Controller, Post, Body, UseGuards, Request, Get, Param, Res } from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';

import { JWTAuthGuard, LocalAuthGuard, Serialize } from 'src/shared';

import { LoginRequestModel, UserModel } from './models';
import { LoginDto, RegisterDto } from './dto';
import { UserService } from './user.service';
import { Response } from 'express';

@ApiTags('user')
@Controller('user')
export class UserController {
	constructor(private readonly service: UserService) {}

	@Post('login')
	@UseGuards(LocalAuthGuard)
	@ApiBody({ type: LoginDto })
	login(@Request() req: LoginRequestModel, @Res() res: Response) {
		return this.service.login(req.user, res);
	}

	@Post('register')
	@Serialize<UserModel>(UserModel)
	register(@Body() body: RegisterDto) {
		return this.service.register(body);
	}

	@Get(':id')
	@UseGuards(JWTAuthGuard)
	@Serialize<UserModel>(UserModel)
	getUser(@Param('id') id: string) {
		return this.service.getUser(+id);
	}
}
