import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '@prisma/client';
import { Response } from 'express';
import * as bcrypt from 'bcrypt';

import { PrismaService } from 'src/prisma/prisma.service';
import { validatePassword } from 'src/shared';

import { PayloadModel } from './models';
import { RegisterDto } from './dto';

@Injectable()
export class UserService {
	constructor(
		private jwtService: JwtService,
		private repository: PrismaService
	) {}

	async validateUserCredentials(email: string, password: string) {
		const user = await this.repository.user.findUnique({
			where: {
				email: email
			}
		});
		if (!user || !(await validatePassword(password, user.password))) {
			return null;
		}
		return user;
	}

	login(user: User, res: Response) {
		const payload: PayloadModel = {
			id: user.id,
			email: user.email
		};

		const accessToken = this.jwtService.sign({
			sub: payload
		});

		const expiresDate = new Date();
		expiresDate.setMonth(expiresDate.getMonth() + 1);

		res.cookie('accessToken', accessToken, {
			expires: new Date(expiresDate)
		});

		return res.send({
			accessToken: accessToken
		});
	}

	async register(body: RegisterDto) {
		const user = await this.repository.user.findUnique({
			where: {
				email: body.email
			}
		});
		if (user) {
			throw new BadRequestException('This email already taken');
		}

		return await this.repository.user.create({
			data: {
				email: body.email,
				firstName: body.firstName,
				lastName: body.lastName,
				password: await bcrypt.hash(body.password, await bcrypt.genSalt())
			}
		});
	}

	async getUser(id: number) {
		const user = await this.repository.user.findUnique({
			where: {
				id: id
			}
		});
		if (!user) {
			throw new NotFoundException('user not found');
		}

		return user;
	}
}
