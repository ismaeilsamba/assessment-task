import { Expose } from 'class-transformer';

export class UserModel {
	@Expose()
	id: number;

	@Expose()
	email: string;

	password: string;

	@Expose()
	firstName: string;

	@Expose()
	lastName: string;

	@Expose()
	createdAt: string;

	@Expose()
	updatedAt: string;
}
