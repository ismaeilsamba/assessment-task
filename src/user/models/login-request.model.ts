import { User } from '@prisma/client';

export class LoginRequestModel {
	user: User;
}
