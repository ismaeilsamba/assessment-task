const socket = io('ws://localhost:4000');

const myPeer = new Peer(undefined, { host: '/', port: '3001' });
const peers = {};

const videoGrid = document.getElementById('video-grid');
const myVideo = document.createElement('video');
myVideo.muted = true;

function connectToNewUser(userId, stream) {
	const call = myPeer.call(userId, stream);
	const video = document.createElement('video');

	call.on('stream', (userVideoStream) => {
		addVideoStream(video, userVideoStream);
	});

	call.on('close', () => {
		video.remove();
	});

	peers[userId] = call;
}

function addVideoStream(video, stream) {
	video.srcObject = stream;
	video.addEventListener('loadedmetadata', () => {
		video.play();
	});
	videoGrid.append(video);
}

navigator.mediaDevices
	.getUserMedia({
		audio: true,
		video: true
	})
	.then((stream) => {
		addVideoStream(myVideo, stream);

		myPeer.on('call', (call) => {
			const video = document.createElement('video');

			call.answer(stream);
			call.on('stream', (userVideoStream) => {
				addVideoStream(video, userVideoStream);
			});
		});

		socket.on('user-connected', (payload) => {
			connectToNewUser(payload.userId, stream);
		});
	});

myPeer.on('open', (id) => {
	socket.emit('join-conference', {
		userId: id,
		conferenceId: CONFERENCE_ID
	});
});

socket.on('user-disconnected', (payload) => {
	if (peers[payload.userId]) {
		peers[payload.userId].close();
		peers[payload.userId] = undefined;
	}
});

socket.on('conference-finished', () => {
	for (let peer of peers) {
		peer.close();
		peer = undefined;
	}
	myPeer.close();
	socket.close();
});
