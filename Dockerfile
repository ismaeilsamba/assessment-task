FROM node:alpine AS development

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --force

COPY . .

RUN npm run build

FROM node:alpine AS production

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
ENV DATABASE_URL="postgresql://misraj:12345678@localhost:5432/misraj?schema=public"
ENV PORT=4000
ENV JWT_SECRET="misraj_jwt_secret_12345678"
ENV EXPIRES_IN="30d"

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install --only=prod --force

COPY . .

COPY --from=development /usr/src/app/dist ./dist

CMD ["node" ,"dist/main"]