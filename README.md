# Misraj Zoom Clone Assessment Task

## Description

Zoom calles are a `stream` of (video/audio) between peers. Each person recieves (and send) `stream` from each person in the call, So we need a `real-time` mechanizem to created this system.

## WebRTC

We will use the open source `WebRTC` to make the system able to send and recieve a `real-time` (video/audio) `stream` between peers. And we will use `Socket.IO` as `Signaling` communicatiion service.

## Use Case

Firstly you have to register and login to be authenticated and able to create a conference.
Then when you create a conference, You have to enter to the url of the conference which is `http://localhost/conference/{conferenceId}` using browser (I'm redirected the creator to the conference but redirect doesn't work in `swagger` so you have to do it manually).
To join an existance conference enter this url in browser from another tab `http://localhost/conference/{conferenceId}`.

## Entity Relationship Diagram (ERD)

![ERD](/docs/diagrams/erd/erd.drawio.png)

## Video/Audio Store

There are many robust and secure solutions to store files. One of them is `AWS-S3` Cloud Storage.
`AWS-S3` provide a links for the files that stored in it, So we can link them to `Conference` table in our database.

## Security

We can determine the `Read/Write` permission in `AWS-S3` to the system only, So if some one need to access to a specifec file he will send the request to our system first, And we can determine here if this person can access to this file. If he can, we get the file from `AWS-S3` and return it as response. In this way we can ensure the `Security` of the files.

## Webhook

Each conference has room assocceated with it in which `roomId = conferenceId`, So when new person join a conference we add his `socket` to the room that has the same id of the conference, Or create a new room if he is the creator of the conference.

## Installation

```bash
$ npm install
$ npm install -g peer
```

## Running the app

```bash
$ npm run start
$ npm run startPeer
```

## Notes

- switch over to the postgres account:

```bash
sudo -i -u postgres
```

- create new role:

```bash
createuser --interactive
Enter name of role to add: misraj
Shall the new role be a superuser? (y/n) y
```

- set misraj role password to 12345678 using

```sql
ALTER USER misraj WITH PASSWORD '12345678';
```

- login to misraj sql role (pass: 12345678):

```bash
psql -h localhost -U misraj
```

- create new database:

```sql
CREATE DATABASE misraj;
```

- run migration file:

```bash
npx prisma migrate dev
```

## Dotenv contents

```bash
DATABASE_URL
PORT
```

## APIs

- Postman collection invitation link:

[Postman Collection](https://app.getpostman.com/join-team?invite_code=08d0391be283de38250adb8bfce46699&target_code=bf4790b755f1e04a962d6dbed328f9a8)

- Swagger API documentation url:

[Swagger Documentation](http://localhost:4000/api)

## Create Meet

- open swagger api
- login
- create conference using POST /conference
- use the url returned and open new windnow with it
- create new window from incognito tab
- login using swagger to another account
- use the same url above

# Deployment

```
NOTE:
This is my first time deploy in Docker + Kubernetes
So Its not finished 100% (sorry for that)
```

## Steps

- First we need to package the application into `Dockerfile`
- Then we can use the `Dockerfile` in `Kubernetes` environment
- Create kubernates cluster
- Create the `deployment.yaml` file and configure it
- Create `docker hub` repository
- `docker login`
- `docker build -t ismailalrifai/assessment-task .`
  After this the docker will build an application that can run inside a container
- `docker push ismailalrifai/assessment-task`
- depoly app locally
  - in k8s folder `kubectl create -f deployment.yaml`
- Create the `service.yaml` file and configure it
  - in k8s folder `kubectl create -f service.yaml`

## CI/CD

![CI/CD](/docs/diagrams/ci-cd/ci-cd.drawio.png)
